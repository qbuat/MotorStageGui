import serial
import time
import sys

class iselFlashMini:


    def moveToPositionAbsolute(self, position, speed=6000):
        sys.stdout.write("Moving to Absolute Position " + str(position) + " at speed " + str(speed) + " . " )
        sys.stdout.flush()
        cmd = "@0M" + str(position) + "," + str(speed) + "\r"
        self.serialPort.write(cmd)
        while self.serialPort.read(10) != '0':
            sys.stdout.write(". ")
            sys.stdout.flush()
        sys.stdout.write("done.\n")
        
        
    def moveToPositionRelative(self, position, speed=6000):
        currentPosition=self.getCurrentPosition()
        sys.stdout.write("Moving from Current Position " + str(currentPosition) + " to Relative Position " + str(position) + " at speed " + str(speed) + " . " )
        sys.stdout.flush()
        cmd = "@0A" + str(position) + "," + str(speed) + "\r"
        self.serialPort.write(cmd)
        while self.serialPort.read(10) != '0':
            sys.stdout.write(". ")
            sys.stdout.flush()
        sys.stdout.write("done.\n")       
        
    def runReferenceRun(self):
        sys.stdout.write("performing reference run . ")
        sys.stdout.flush()
        self.serialPort.write("@0R1\r")
        while self.serialPort.read(10) != '0':
            sys.stdout.write(". ")
            sys.stdout.flush()
        sys.stdout.write("done.\n")
        sys.stdout.flush()        
        
    def getCurrentPosition(self):
        self.serialPort.write("@0P\r")
        return int(self.serialPort.read(10), 16)
    
    def setReferenceSpeed(self, speed=6000):
        sys.stdout.write("setting reference speed . ")
        sys.stdout.flush()
        cmd = "@0d" + str(speed) + "\r"
        self.serialPort.write(cmd)
        while self.serialPort.read(10) != '0':
            sys.stdout.write(". ")
            sys.stdout.flush()
        sys.stdout.write("done.\n")
        
    def configureController(self, numberOfAxis=1):
        sys.stdout.write("configurating controller . " )
        self.serialPort.read(10)
        sys.stdout.flush()
        cmd = "@0" + str(numberOfAxis) + "\r"
        self.serialPort.write(cmd)
        while self.serialPort.read(10) != '0':
            sys.stdout.write(". ")
            sys.stdout.flush()
        sys.stdout.write("done.\n")
     


    def __init__(self, port, baudrate=19200, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, timeout=2 ):
        self.serialPort = serial.Serial( port=port, baudrate=baudrate, bytesize=bytesize, parity=parity, timeout=timeout)
        
        try:
            sys.stdout.write("trying to open serial port . . . ")
            sys.stdout.flush()
            self.serialPort.isOpen()
            sys.stdout.write("done.\n")
            sys.stdout.flush()
           
        except Exception, e:
            print ("error opening serial port: " + str(e))
            exit()
        
        try:
            self.configureController(1)
            self.setReferenceSpeed(6000)
            self.runReferenceRun()
            
        except Exception, e:
            print ("error configuring controller " + str(e))
            exit()
        