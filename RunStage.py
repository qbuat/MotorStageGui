import serial
import sys
import time
import iselFlashMini
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QCheckBox,QLineEdit
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
from functools import partial

class App(QWidget):
    def __init__(self):
        super(App, self).__init__()
        self.title = "PyQt5 Stage Control"
        self.left = 50
        self.top = 50
        self.width = 1800
        self.height = 400
        self.initUI()
        self.motorstage = iselFlashMini.iselFlashMini(port='COM4')
        
        self.updatePositionDisplay()
    
    def updatePositionDisplay(self):
        curPos = int(self.motorstage.getCurrentPosition())
        dispText = "Current Position: " + str(curPos)
        self.displayPosition.setText(dispText) 
    
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        self.checkboxLocked = QCheckBox("Stage Locked",self)
        self.checkboxLocked.move(300, 35)
        self.checkboxLocked.setChecked(True)
        
        self.displayPosition = QLineEdit(self)
        self.displayPosition.move(500, 35)


        positions = [1000, 6000, 10000, 14000, 18000, 22000, 26000, 30000, 34000, 38000, 42000, 46000, 50000, 54000, 58000, 62000, 66000, 70000, 78000, 86000, 94000, 102000, 110000, 118000, 126000, 134000, 142000, 150000, 158000, 166000, 174000, 178000, 182000, 186000, 190000, 194000, 198000, 202000, 206000, 210000, 214000, 218000, 222000, 226000, 230000, 234000, 238000]
        buttons = []
        for i in range(0,47):
            if i == 0 :
                bname = "Park Position"
                bx = 100
                by = 35
                
            if i > 0 and i <= 16 :
                bname = "AX+_IM" + str(i)
                bx = 100*i
                by = 105
      
            if i > 16 and i <= 23 :
                bname = "AX+_BM" + str(i-16)
                bx = 100*(i-16)
                by = 175
            
            if i > 23 and i <= 30 :
                bname = "AX-_BM" + str(i-23)
                bx = 100*(i-23)
                by = 245

            if i > 30 :
                bname = "AX-_IM" + str(i-30)
                bx = 100*(i-30)
                by = 305
                
            button = QPushButton(bname, self)
            button.setToolTip('Move to ' + bname)
            button.move(bx, by)
            position=positions[i]
            button.clicked.connect(partial(self.on_click_b, position))
            
            
        button = QPushButton("Update Position", self)
        button.setToolTip('Update Current Slide Location')
        button.move(415, 35)
        button.clicked.connect(self.on_click_update)
            
        self.show()
        
    @pyqtSlot()
    def on_click_b(self, position):
        if( self.checkboxLocked.isChecked() ) :
            print("Motorstage is Locked")
        else :
            try:
                self.motorstage.moveToPositionAbsolute(position)
                self.updatePositionDisplay()

            except Exception, e:
                print("error" + str(e))
                
    @pyqtSlot()
    def on_click_update(self):
        self.updatePositionDisplay()



if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = App()    
    sys.exit(app.exec_())